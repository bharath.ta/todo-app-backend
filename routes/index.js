var express = require('express');
var router = express.Router();
const { Todo } = require('../database/schema');

/* GET all TODO. */
router.get('/', function(req, res, next) {
  Todo.find((err,todos) => {
    if(err) 
      res.status(400).json("error : ",err);
    res.status(200).json(todos);
  });
});

/* add TODO. */
router.post('/add', (req,res,next) => {
  console.log('ADD TODO: ', req.body)
  const newTodo = new Todo({
    todo : req.body.todo,
    completed : false
  });

  newTodo.save((err,todo) => {
    if(err)
      res.status(400).json("something went wrong:",err);
    res.status(200).json({message:"todo is inserted",todo});
  });
});

/* delete TODO. */
router.delete('/delete' , (req,res,next) => {
  Todo.findByIdAndDelete({_id : req.body._id} , (err,todo) => {
    if(err)
      res.status(400).json("error in deleting")
      res.status(200).json({meussage : 'deleted the todo',todo})
  });
});

/* Find by id and update status of completed */
router.post('/update' , (req,res,next) => {
  Todo.findByIdAndUpdate({_id : req.body._id}, { completed : req.body.completed } , (err,todo) => {
    if(err)
      res.status(400).json("error in updating")
      res.status(200).json({message : 'updated todo',todo})
  });
});


module.exports = router;
