const mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect(process.env.mongoURI, {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

const connection = mongoose.connection;

connection.on('connected', () => console.log('connected to mongoDB successfully'));
connection.on('disconnected', () => console.log('disconnected from mongoDB'));
connection.on('error', () => console.log('Something went wrong, please re-connect'));

//unexpected shutdown
process.on("SIGINT",()=>connection.on(()=>process.exit(0)));