const mongoose =require('mongoose');
const schema  = mongoose.Schema;
const todoSchema = new schema({
    todo : { type : String , required : true},
    completed : { type : Boolean , required : true }
});

const Todo = mongoose.model('Todo' , todoSchema);

module.exports.Todo = Todo;