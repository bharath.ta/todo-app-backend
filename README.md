**WHY TODO APP?**

TODO app was developed as a part of react native learning. This app uses MongoDB to store the TODOs and update their states or delete them. CRUD operations can be performed with this app, concepts of useState, useEffect, Checkbox, ScrollView, SafeAreaView,etc were implemented in this.

**Note:**

This app will work when both frontend and backend servers are up and running.
Please add .env file in backend with your database URL for this to work.

  `mongoURI=<your mongoDB URI and change the password>`

Please make necesssary changes in the fetch api's based on the port number you are running your backend.

example : `fetch("http://localhost:3000/") if you are runnning the backend in port 3000`
